#######################################################################################
#     output specific subnet id
########################################################################################
output "specific_id" {
    value = aws_subnet.private_subnets["private_subnet_1"].id  
}

#######################################################################################
#     output all subnet ids create from the resource orivate_subnets
########################################################################################

output "all_sub_id" {
    value = [for value in aws_subnet.private_subnets:value.id]
}
