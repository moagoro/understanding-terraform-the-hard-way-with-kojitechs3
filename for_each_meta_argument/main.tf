########################################################################################
#### local block ####
#######################################################################################

locals {
azs=data.aws_availability_zones.available.names # pull down all AZs

########################################################################################
#### private subnet key and value declaration for use with for-each meta argurment #####
public_subnets={
   public_subnet_1={
     cidr="10.0.0.0/24"
     avail_zone=local.azs[0]
   }
   public_subnet_2={
     cidr="10.0.2.0/24"
     avail_zone=local.azs[1]
   }
}
########################################################################################
#### private subnet key and value declaration for use with for-each meta argurment #####
private_subnets ={
    private_subnet_1={
     cidr="10.0.101.0/24"
     avail_zone=local.azs[0]
   }
   private_subnet_2={
     cidr="10.0.103.0/24"
     avail_zone=local.azs[1]
   }
}

########################################################################################
#### database subnet key and value declaration for use with for-each meta argurment #####
database_subnets ={
    database_subnet_1={
     cidr="10.0.105.0/24"
     avail_zone=local.azs[0]
   }
   database_subnet_2={
     cidr="10.0.107.0/24"
     avail_zone=local.azs[1]
   }
}
}

#######################################################################################
#     create VPC
########################################################################################
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

#######################################################################################
#     create 2 public subnets
########################################################################################
resource "aws_subnet" "public_subnets" {
    for_each = local.public_subnets    ## use for each meta aragument which uses map
                       
   vpc_id     = aws_vpc.this.id
   cidr_block = each.value.cidr
   availability_zone = each.value.avail_zone
   map_public_ip_on_launch= true

  tags = {
    Name =each.key # use concantenate sign ${} to make the name unique
  }
}

#######################################################################################
#     create 2 private subnets
########################################################################################
resource "aws_subnet" "private_subnets" {
    for_each = local.private_subnets    ## use for-each meta aragument which defined map in the local block
    vpc_id     = aws_vpc.this.id   
    cidr_block = each.value.cidr
    availability_zone = each.value.avail_zone
  
  tags = {
    Name =each.key # use concantenate sign ${} to make the name unique
  }
}


#######################################################################################
#     create 2 database subnets
########################################################################################

resource "aws_subnet" "database_subnet" {
    for_each = local.database_subnets    ## use for-each meta aragument which defined map in the local block
    vpc_id     = aws_vpc.this.id   
    cidr_block = each.value.cidr
    availability_zone = each.value.avail_zone
  
  tags = {
    Name =each.key # use concantenate sign ${} to make the name unique
  }
}
