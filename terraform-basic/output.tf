

#output "server_id" {
#  description = "Private DNS Id"
#  value = aws_instance.ec2_instance.private_dns
#}


#get the vpc id from the module
/*output "vpc_id" {
  description = "output the vpc id from module vpc"
  value = module.vpc.vpc_id
}

#get the public subnet from the module
output "public_subnet" {
  description = "output the public subnet from module vpc"
  value = module.vpc.public_subnets
}*/


output "vpc_id" {
  description = "output the vpc id from resource vpc block"
  value = aws_vpc.vpc.id
}