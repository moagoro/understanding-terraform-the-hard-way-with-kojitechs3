
##################    Terraform Block  ############################33
terraform {
  required_version = ">=1.1.0"
  required_providers {
      aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0" # Optional but recommended in production
    }
  }
}
############ Provider Block #####################
provider "aws" {
  profile = "terraform-user"  #Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
  region  = "us-east-1"
}

#################  RESOURCE BLOCKS ###################

resource "aws_instance" "ec2_instance" {
  ami           = data.aws_ami.ami.id # use datasource to pull down the ami from the console
  instance_type = "t2.micro"
  tags = {
     Name = "EC2_Instance"
  }
}

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc-cidr
}

resource "aws_subnet" "private" {

  count= length(var.private_subnets) # 5 subnets
  vpc_id     = local.vpc_id
  cidr_block = var.private_subnets[count.index] # count.index will pick the subnets list one at a time
  availability_zone = element(slice(local.azs, 0,2), count.index)#use locals block for the az list and 
                                                        #slice to select the first 2 and use element function to itirate both lsit since there is more subnet than azs
 tags = {
    Name = "Private_Subnet"
}

}

/*resource "aws_subnet" "public1" {
  vpc_id     = local.vpc_id
  cidr_block = var.public_subnets[0]
  availability_zone = data.aws_availability_zones.available.names[0] #us west region

}

resource "aws_subnet" "public2" {
  vpc_id     = local.vpc_id
  cidr_block = var.public_subnets[1]
  availability_zone = data.aws_availability_zones.available.names[1] #us west region

}*/











