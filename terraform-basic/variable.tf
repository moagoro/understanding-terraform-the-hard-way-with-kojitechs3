
##variable block
#create a variable
/*variable "ami_id" {
 type=string
 description="ami-id"
 default= "ami-09d3b3274b6c5d4aa" 
}*/

variable "vpc-cidr" {
  type=string
  description="vpc-cidr"
   default= "10.0.0.0/16"
}



/*variable "vpc_cidr_id" {
  type=list
  description="vpc-cidr"
   default= ["10.0.0.0/16","10.0.1.0/24", "10.0.2.0/24","10.0.3.0/24"]
}*/

variable "private_subnets" {
  type=list
  description="Private_subnets"
   default=["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24", "10.0.4.0/24", "10.0.7.0/24"]
}

variable "public_subnets" {
  type=list
  description="public_subnets"
   default=["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24", "10.0.104.0/24"]
}
