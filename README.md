


#
-terraform init
-terraform validate
-terraform plan
-terraform apply
-terraform destroy
# terraform console


# """"""

# " " :> string
# [] = list 
# 80 = number 
# bool = true/false 
# {}  =  map 

# ### complicated
# [""] => list(string)
# [{}] => list(map)
# {[]} => map(list)
# """""
