

###### VPC resource ####
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"
}

##### 2 public subnets resource blk ####
resource "aws_subnet" "public_subnets" {
    
  count=length(var.public_sub_cidr)
  vpc_id     = aws_vpc.this.id
  cidr_block = var.public_sub_cidr[count.index]
  availability_zone = slice(local.azs, 0,2)[count.index]
  map_public_ip_on_launch= true

  tags = {
    Name = "Public_subnet_${count.index + 1}" # use concantenate sign ${} to make the name unique
  }
}



