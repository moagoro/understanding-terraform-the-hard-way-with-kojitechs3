

############ Provider Block #####################
provider "aws" {
  profile = "terraform-user"  #Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
  region  = "us-east-1"
}